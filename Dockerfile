FROM  jupyter/base-notebook

EXPOSE 8888

CMD ['jupyter', 'lab', '--ip=0.0.0.0', '--no-browser', '--port=8888']